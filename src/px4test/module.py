from __future__ import annotations

from importlib import import_module
from os import getcwd
from sys import path
from typing import Optional, Any

from attr import attrs, attrib, fields_dict
from attr.validators import instance_of
from attr.converters import default_if_none
from px4stack.config import Px4StackConfig
from staliro.options import Options
from staliro.optimizers import Optimizer
from staliro.specification import Specification

from .config import Px4TestConfig
from .blackbox import MissionFactory

path.append(getcwd())


@attrs(auto_attribs=True, frozen=True)
class Module:
    specification: Specification
    optimizer: Optimizer[Any]
    options: Options
    stack_config: Px4StackConfig = attrib(
        factory=Px4StackConfig,
        converter=default_if_none(factory=Px4StackConfig),  # type: ignore
        validator=instance_of(Px4StackConfig),
    )
    config: Px4TestConfig = attrib(
        factory=Px4TestConfig,
        converter=default_if_none(factory=Px4TestConfig),  # type: ignore
        validator=instance_of(Px4TestConfig),
    )
    mission_factory: Optional[MissionFactory] = None


def load_module(module_name: str) -> Module:
    module = import_module(module_name)
    fields = fields_dict(Module).keys()
    module_attrs = {field: getattr(module, field, None) for field in fields}

    return Module(**module_attrs)
