from __future__ import annotations

from pathlib import Path
from typing import Optional, Any

from attr import attrs, attrib, Attribute
from attr.validators import instance_of, optional


def _validate_delay(instance: Any, attr: Attribute[Any], value: Any) -> None:
    if not isinstance(value, int):
        raise TypeError("Only ints are supported as delay values")

    if value < 1:
        raise ValueError("Delay must be positive integer")


@attrs(auto_attribs=True, frozen=True)
class Px4TestConfig:
    log_dest: Optional[Path] = attrib(None, validator=instance_of(Path))
    mission_dest: Optional[Path] = attrib(None, validator=instance_of(Path))
    delay_ms: Optional[int] = attrib(None, validator=optional(_validate_delay))
