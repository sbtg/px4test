from typing import BinaryIO, Dict, Union

from numpy import float_, int_
from numpy.typing import NDArray

class ULog:
    class Data:
        data: Dict[str, NDArray[Union[float_, int_]]]
    def __init__(self, log_file: Union[str, BinaryIO]): ...
    def get_dataset(self, name: str, multi_instance: int = ...) -> Data: ...
