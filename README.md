# px4test

This is a harness for running falsification tests against the [px4](https://px4.io) autopilot. This package uses docker
to start a px4 simulation and [s-taliro](https://gitlab.com/sbtg/pystaliro) to check the system output against a
provided specification.
